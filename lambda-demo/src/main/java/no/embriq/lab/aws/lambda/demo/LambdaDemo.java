package no.embriq.lab.aws.lambda.demo;

import java.io.IOException;

import no.embriq.lab.aws.util.lambda.RequestHandler;

/**
 * @author Sindre Mehus
 */
public class LambdaDemo extends RequestHandler {

    @Override
    public void invoke() throws IOException {
        info("Environment variables:");
        System.getenv().forEach((k, v) -> info(k + ": " + v));
    }

    private void info(String value) {
        log(LogType.INFO, value);
    }
}
