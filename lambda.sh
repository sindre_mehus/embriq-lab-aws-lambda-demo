#!/bin/sh -e

export AWS_DEFAULT_REGION=eu-west-1

FUNCTION_NAME=
ZIP_FILE=
S3_BUCKET=
ALIAS=

usage() {
    echo "Updates the code for the specified Lambda function, optionally updating an alias to point to the new version."
    echo
    echo "Usage: lambda.sh --function-name=<value> --zip-file=<value> --s3-bucket=<value> [--alias=<value>]"
    echo "  --help                  This small usage guide."
    echo "  --function-name=<value> The existing Lambda function name whose code you want to replace."
    echo "  --zip-file=<value>      The path to the zip (or jar) file of the code you are uploading."
    echo "  --s3-bucket=<value>     Deploy the Lambda using this S3 bucket."
    echo "  --alias=<value>         Assign this alias to the version of the newly updated function code."
    exit 1
}

# Parse arguments.
while [ $# -ge 1 ]; do
    case $1 in
        --help)
            usage
            ;;
        --function-name=?*)
            FUNCTION_NAME=${1#--function-name=}
            ;;
        --zip-file=?*)
            ZIP_FILE=${1#--zip-file=}
            ;;
        --s3-bucket=?*)
            S3_BUCKET=${1#--s3-bucket=}
            ;;
        --alias=?*)
            ALIAS=${1#--alias=}
            ;;
        *)
            usage
            ;;
    esac
    shift
done

if [ ! ${FUNCTION_NAME} ]; then
    echo "Missing function name (--function-name=<value>)"
    exit
fi
if [ ! "${ZIP_FILE}" ]; then
    echo "Missing zip file (--zip-file=<value>)"
    exit
fi
if [ ! ${S3_BUCKET} ]; then
    echo "Missing S3 bucket (--s3-bucket=<value>)"
    exit
fi

S3_KEY=$(basename "${ZIP_FILE}")
echo "Uploading ${ZIP_FILE} to S3..."
aws s3 cp "${ZIP_FILE}" "s3://${S3_BUCKET}/${S3_KEY}"

echo
echo "Updating lambda code for function ${FUNCTION_NAME}..."
VERSION=$(aws lambda update-function-code --function-name ${FUNCTION_NAME} --s3-bucket ${S3_BUCKET} --s3-key "${S3_KEY}" --publish --output text --query Version)
echo "Updated lambda code to version ${VERSION}"

if [ ${ALIAS} ]; then
    echo "Updating lambda alias ${ALIAS}..."
    aws lambda update-alias --function-name ${FUNCTION_NAME} --function-version ${VERSION} --name ${ALIAS}
    echo "Updated lambda alias ${ALIAS} to point to version ${VERSION}"
fi

